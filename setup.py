import sys
import setuptools
from setuptools.command.test import test as TestCommand

class PyTest(TestCommand):
	user_options = [
		('filter=', 'k', "Test filter")
	]
	
	def initialize_options(self):
		super().initialize_options()
		self.filter = ''
	
	def run_tests(self):
		import pytest
		sys.exit(pytest.main(['-k', self.filter] if self.filter else []))

tests_require = ['pytest']

with open('README.md', 'r') as fh:
	long_description = fh.read()

setuptools.setup(
	name = 'chong',
	version = '0.2.0',
	author = "valtron",
	description = "A very simple, hackable parser generator.",
	long_description = long_description,
	long_description_content_type = 'text/markdown',
	url = 'https://gitlab.com/valtron/chong',
	packages = ['chong'],
	python_requires = '>=3.5',
	install_requires = [],
	tests_require = tests_require,
	extras_require = { 'test': tests_require },
	cmdclass = { 'test': PyTest },
	classifiers = [
		'Programming Language :: Python :: 3 :: Only',
		'Programming Language :: Python :: 3.5',
		'Programming Language :: Python :: 3.6',
		'Programming Language :: Python :: 3.7',
		'Operating System :: OS Independent',
		'License :: OSI Approved :: MIT License',
	],
)
