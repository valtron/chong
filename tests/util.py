from typing import List, Any, Set, Dict

from chong.models import NonTerm, Sym
from chong.specfile import Spec
from chong.algos import ParseTables, Action, Shift, Reduce
from chong.codegen import GLRParser, Reduction

def assertParses(spec: Spec, input: str, trees: List[Any]) -> None:
	parser = parser_from_spec(spec)
	res = parse(input, parser)
	assert set(res) == { _process_tree(spec.symbols, tree) for tree in trees }, res

def parser_from_spec(spec: Spec) -> GLRParser:
	parse_tables = ParseTables.FromSpec(spec)
	parse_tables = ParseTables(_translate_reductions(parse_tables.actions), parse_tables.goto)
	return GLRParser(parse_tables)

def parse(input: str, parser: GLRParser) -> List[Any]:
	for tok in input:
		parser.process(tok)
	parser.process('$')
	return parser.res

def _process_tree(symbols: Dict[str, Sym], tree: Any) -> Any:
	if isinstance(tree, str): return tree
	return (symbols[tree[0]], tuple(_process_tree(symbols, t) for t in tree[1]))

def _translate_reductions(actions: Dict[Any, Set[Any]]) -> Dict[Any, Set[Any]]:
	return {
		k: { _translate_reduction(a) for a in acts }
		for k, acts in actions.items()
	}

def _translate_reduction(action: Action) -> Action:
	if isinstance(action, Shift): return action
	nt = action.nonterm
	return Reduce(nt, TestReduction(action.reduction.rule_len, nt))

class TestReduction(Reduction):
	__slots__ = ('nt',)
	
	nt: NonTerm
	
	def __init__(self, rule_len: int, nt: NonTerm) -> None:
		super().__init__(rule_len)
		self.nt = nt
	
	def reduce(self, values: List[Any]) -> Any:
		return (self.nt, tuple(values))
