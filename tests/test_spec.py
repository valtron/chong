import pytest

from chong.specfile import Spec, SpecParseError
from .util import assertParses, parse

def test_1() -> None:
	spec = Spec.ParseText('''%start Foo

  # comment

Foo: class x:Token? y:Bar[]?
| bar_list x_tok:x x? bar_list:y

bar_list: list Bar sep:,

Bar: union
| y
| z

x_tok: Token
| x
''')
	
	s = spec.symbols
	
	assertParses(spec, 'xy,z,y', [
		('Foo', [
			('bar_list', []),
			('x_tok', ['x']),
			('x.opt', []),
			('bar_list', [
				('bar_list.nonempty', [
					('bar_list.nonempty', [
						('bar_list.nonempty', [
							('Bar', ['y']),
						]),
						',',
						('Bar', ['z']),
					]),
					',',
					('Bar', ['y']),
				]),
			])
		])
	])

def test_fails_circular_list() -> None:
	_assert_fails_circular('''%start X\nX: list X''', 'X')
	_assert_fails_circular('''%start X\nX: list Y\nY: list X''', 'XY')

def test_fails_circular_class() -> None:
	_assert_fails_circular('''%start X\nX: class X''', 'X')
	_assert_fails_circular('''%start X\nX: class Y\nY: class X''', 'XY')

def _assert_fails_circular(spectxt: str, names: str) -> None:
	with pytest.raises(SpecParseError) as ex_info:
		Spec.ParseText(spectxt)
	msg = ex_info.value.args[0]
	assert "circular type dependency" in msg
	assert repr(list(names)) in msg
