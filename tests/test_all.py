from typing import Tuple, List, Any, Dict
from pathlib import Path
import pytest

from chong.codegen import GLRParser, TooManyStatesError
from chong.specfile import Spec

from .util import assertParses, parse, parser_from_spec

class TestGLR:
	def test_file_0(self) -> None:
		spec = Spec.ParseFile(Path('tests/file0.txt'))
		assertParses(spec, 'ab', [
			('list', [
				('list', [
					('list', []),
					('term', ['a']),
				]),
				('term', ['b']),
			]),
		])
	
	def test_infinite_0(self) -> None:
		spec = _rules_to_spec([
			('A', ['A']),
			('A', []),
		])
		
		parser = parser_from_spec(spec)
		with pytest.raises(TooManyStatesError):
			parse('', parser)
	
	def test_nullable_0(self) -> None:
		spec = _rules_to_spec([
			('A', ['E', 'E', 'E']),
			('E', []),
		])
		
		assertParses(spec, '', [
			('A', [('E', [])] * 3),
		])
	
	def test_nullable_1(self) -> None:
		spec = _rules_to_spec([
			('A', ['B', 'x']),
			('B', []),
		])
		
		assertParses(spec, 'x', [
			('A', [('B', []), 'x']),
		])
	
	def test_nullable_2(self) -> None:
		spec = _rules_to_spec([
			('S', ['A', 'S', 'b']),
			('S', ['x']),
			('A', []),
		])
		
		expected_parse = [
			('S', [
				('A', []),
				('S', [
					('A', []),
					('S', ['x']),
					'b',
				]),
				'b',
			]),
		]
		
		try:
			assertParses(spec, 'xbb', expected_parse)
		except TooManyStatesError:
			raise pytest.skip("certain nullable parses not implemented")
	
	def test_ambig(self) -> None:
		spec = _rules_to_spec([
			('E', ['E', 'E']),
			('E', ['a']),
		])
		
		assertParses(spec, 'aaa', [
			('E', [
				('E', [
					('E', ['a']),
					('E', ['a']),
				]),
				('E', ['a']),
			]),
			('E', [
				('E', ['a']),
				('E', [
					('E', ['a']),
					('E', ['a']),
				]),
			]),
		])
	
	def test_tough(self) -> None:
		spec = _rules_to_spec([
			('E', ['A', 'a']),
			('E', ['B', 'b']),
			('A', []),
			('A', ['A', 'x']),
			('B', []),
			('B', ['B', 'x']),
		])
		
		assertParses(spec, 'xxxa', [
			('E', [
				('A', [
					('A', [
						('A', [('A', []), 'x']),
						'x',
					]),
					'x',
				]),
				'a',
			]),
		])
	
	def test_expr(self) -> None:
		spec = _rules_to_spec([
			('E', ['E', '+', 'T']),
			('E', ['T']),
			('T', ['T', '*', 'F']),
			('T', ['F']),
			('F', ['(', 'E', ')']),
			('F', ['x']),
		])
		
		assertParses(spec, 'x+x*(x+x)', [
			('E', [
				('E', [
					('T', [('F', ['x'])]),
				]),
				'+',
				('T', [
					('T', [('F', ['x'])]),
					'*',
					('F', [
						'(',
						('E', [
							('E', [
								('T', [('F', ['x'])]),
							]),
							'+',
							('T', [('F', ['x'])]),
						]),
						')',
					]),
				]),
			]),
		])

def _rules_to_spec(rules: List[Tuple[str, List[str]]]) -> Spec:
	from chong.models import NonTerm, Term, Rule, TToken, Sym, Reduction
	
	symbols: Dict[str, Sym] = {}
	for head, tail in rules:
		if head in symbols: continue
		symbols[head] = NonTerm(head, TToken, [])
	
	for head, tail in rules:
		rule = Rule([], Reduction(len(tail), TToken))
		for ti in tail:
			sym = symbols.get(ti)
			if sym is None:
				sym = Term(ti)
				symbols[ti] = sym
			rule.body.append(sym)
		nt = symbols[head]
		assert isinstance(nt, NonTerm)
		nt.rules.append(rule)
	
	return Spec(rules[0][0], symbols, {})
