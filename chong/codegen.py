from typing import Dict, Tuple, Set, List, Any, Optional

from .models import Term, NonTerm
from .algos import Reduce, StateID, ParseTables, AcceptStateID

TableKey = Tuple[StateID, str]

class GLRParser:
	_goto: Dict[TableKey, StateID]
	_shift: Dict[TableKey, StateID]
	_reduce: Dict[TableKey, Set[Any]]
	_has_shifts: Set[StateID]
	res: List[Any]
	
	def __init__(self, parse_tables: ParseTables) -> None:
		self._goto = {}
		self._shift = {}
		self._reduce = {}
		self._has_shifts = set()
		
		for (sid, tok), acts in parse_tables.actions.items():
			for a in acts:
				if isinstance(a, Reduce):
					if (sid, tok.name) not in self._reduce:
						self._reduce[(sid, tok.name)] = set()
					self._reduce[(sid, tok.name)].add((a.nonterm, a.reduction))
				else:
					self._shift[(sid, tok.name)] = a.next_state
					self._has_shifts.add(sid)
		
		for (sid1, nt), sid2 in parse_tables.goto.items():
			self._goto[(sid1, nt.name)] = sid2
		
		self.states = [State(StateID(0), None)]
		self.res = []
		
		self._maxstates = 100
	
	def process(self, tok: Any) -> None:
		self.reduce(tok)
		self.shift(tok)
	
	def shift(self, tok: Any) -> None:
		newstates: List[State] = []
		for st in self.states:
			k = (st.id, tok)
			nst = self._shift.get(k)
			if nst is None: continue
			if nst == AcceptStateID:
				self.res.append(st.value)
				continue
			newstates.append(State(nst, tok, st))
		self.states = newstates
	
	def reduce(self, tok: Any) -> None:
		newstates: List[State] = []
		
		unhandled = self.states
		while unhandled:
			unhandled_next: List[State] = []
			
			for st in unhandled:
				for (nt, reduction) in self._reduce.get((st.id, tok), []):
					children, z = _popstack(st, reduction.rule_len)
					value = reduction.reduce(children)
					nst = self._goto[(z.id, nt.name)]
					unhandled_next.append(State(nst, value, z))
				
				if st.id in self._has_shifts:
					newstates.append(st)
			
			if len(newstates) >= self._maxstates:
				raise TooManyStatesError()
			
			unhandled = unhandled_next
		
		self.states = newstates

class TooManyStatesError(Exception):
	pass

class Reduction:
	__slots__ = ('rule_len',)
	
	rule_len: int
	
	def __init__(self, rule_len: int) -> None:
		self.rule_len = rule_len
	
	def reduce(self, values: List[Any]) -> None:
		raise NotImplementedError('Reduction.reduce')

class State:
	def __init__(self, id: StateID, value: Any, parent: Optional['State'] = None) -> None:
		self.id = id
		self.value = value
		self.parent = parent
	
	def __str__(self) -> str:
		if self.id == 0:
			return '0'
		return '{}: {} [{}]'.format(self.id, self.value, self.parent or '')
	
	def __repr__(self) -> str:
		return '<#{}>'.format(str(self))

def _popstack(st: State, k: int) -> Tuple[List[Any], State]:
	children = []
	z = st
	while k > 0:
		children.append(z.value)
		assert z.parent is not None
		z = z.parent
		k -= 1
	return list(reversed(children)), z
