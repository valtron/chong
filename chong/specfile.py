from typing import Dict, Any, Optional, List, Iterable, Tuple
from pathlib import Path
import re

from .models import (
	Rule, Term, NonTerm, Sym,
	TSyntax, TClass, TUnion, TList, TOptional, TToken, TNull,
	RNew, RListZero, RListOne, RListAppend, RWrap, RConst, Reduction,
)

class Spec:
	__slots__ = ('start', 'symbols', 'named_types')
	
	start: str
	symbols: Dict[str, Sym]
	named_types: Dict[str, TSyntax]
	
	@classmethod
	def ParseFile(cls, path: Path) -> 'Spec':
		parser = _SpecParser(str(path))
		with path.open('r') as fh:
			return parser.parse(fh)
	
	@classmethod
	def ParseText(cls, text: str, *, source: str = '<string>') -> 'Spec':
		parser = _SpecParser(source)
		return parser.parse(text.splitlines())
	
	def __init__(self, start: str, symbols: Dict[str, Sym], named_types: Dict[str, TSyntax]) -> None:
		self.start = start
		self.symbols = symbols
		self.named_types = named_types

class SpecParseError(ValueError):
	pass

class _SpecParser:
	__slots__ = (
		'source', 'indent', 'lineno', 'nonterms_raw', 'nonterms', 'symbols',
		'builtin_types', 'named_types', 'optional_types', 'list_types',
		'type_build_stack',
	)
	
	source: str
	indent: str
	lineno: int
	nonterms_raw: Dict[str, Any]
	nonterms: Dict[str, NonTerm]
	symbols: Dict[str, Sym]
	builtin_types: Dict[str, TSyntax]
	named_types: Dict[str, TSyntax]
	optional_types: Dict[TSyntax, TOptional]
	list_types: Dict[TSyntax, TList]
	type_build_stack: List[str]
	
	def __init__(self, source: str, *, indent: str = '\t') -> None:
		self.source = source
		self.indent = indent
		self.lineno = 0
		self.nonterms_raw = {}
		self.nonterms = {}
		self.symbols = {}
		self.builtin_types = {
			'Token': TToken,
		}
		self.named_types = {}
		self.optional_types = {}
		self.list_types = {}
		self.type_build_stack = []
	
	def parse(self, line_iterator: Iterable[str]) -> Spec:
		start = self._read_lines(line_iterator)
		self._create_list_rules()
		self._create_nonterms_with_types()
		self._create_nonterm_rules()
		return Spec(start, self.symbols, self.named_types)
	
	def _read_lines(self, line_iterator: Iterable[str]) -> str:
		start: Optional[str] = None
		
		start_lineno: Optional[int] = None
		current_nonterm = None
		current_rule = None
		for lineno, line in enumerate(line_iterator):
			self._set_lineno(lineno)
			
			if line.startswith('#'):
				continue
			
			if line.startswith(self.indent):
				line = line.strip()
				if line.startswith('#'): continue
				if current_rule is None:
					raise self._error("reduction specified, but no rule active")
				current_rule['reduction'] = line
				current_rule = None
				continue
			
			if line.startswith('|'):
				if current_nonterm is None:
					raise self._error("rule specified, but no nonterm active")
				current_rule = {
					'lineno': lineno,
					'parts': [_split_rulepart(s) for s in _split_spaces(line[1:])],
					'reduction': None,
				}
				current_nonterm['rules'].append(current_rule)
				continue
			
			if line.startswith('%'):
				(name, value) = _split_two(line[1:], ' ')
				if name != 'start':
					raise self._error("'start' is the only supported pragma (got {!r})", name)
				if not value:
					raise self._error("'start' pragma needs the name of a nonterminal")
				if start is not None:
					raise self._error("start symbol set multiple times")
				start = value
				start_lineno = lineno
				continue
			
			line = line.strip()
			if not line:
				current_nonterm = None
				continue
			if line.startswith('#'): continue
			
			nt_name, typespec = _split_two(line, ':')
			current_nonterm = { 'lineno': lineno, 'typespec': _parse_arglike(typespec), 'rules': [] }
			self.nonterms_raw[nt_name] = current_nonterm
		
		if start is None:
			raise self._error("start nonterminal not specified")
		else:
			assert start_lineno is not None
			self._set_lineno(start_lineno)
		
		if start not in self.nonterms_raw:
			raise self._error("start symbol {!r} is not a nonterminal", start)
		
		return start
	
	def _create_nonterms_with_types(self) -> None:
		for name, data in self.nonterms_raw.items():
			self._set_lineno(data['lineno'])
			_ = self._get_sym(name)
			ts = data['typespec']
			first = ts[0][0]
			
			# Adds raw reductions
			for raw_rule in data['rules']:
				if raw_rule['reduction']: continue
				if first == 'class':
					raw_rule['reduction'] = 'new {}'.format(name)
					continue
				# `list` nonterms should have had reductions generated in `_create_list_rules`
				assert first != 'list'
				raw_rule['reduction'] = 'wrap'
			
			# field types
			if first != 'class': continue
			t = self.nonterms[name].type
			assert isinstance(t, TClass)
			for k, v in ts[1].items():
				t.field_types[k] = self._get_type(v)
	
	def _create_nonterm_rules(self) -> None:
		for name, data in self.nonterms_raw.items():
			nonterm = self.nonterms[name]
			if data['typespec'][0][0] == 'union':
				assert isinstance(nonterm.type, TUnion)
				union_type: Optional[TUnion] = nonterm.type
			else:
				union_type = None
			for rule_raw in data['rules']:
				self._set_lineno(rule_raw['lineno'])
				rr = _split_spaces(rule_raw['reduction'])
				if len(rr) < 1: raise self._error("invalid reduction")
				parts = rule_raw['parts']
				first = rr[0]
				if first == 'list':
					if len(rr) != 2: raise self._error("invalid 'list' reduction")
					rule = self._process_list_reduction(rr[1], nonterm, parts)
				elif first == 'new':
					if len(rr) != 2: raise self._error("invalid 'new' reduction")
					rule = self._process_new_reduction(rr[1], nonterm, parts)
				elif first == 'const':
					if len(rr) != 2: raise self._error("invalid 'const' reduction")
					rule = self._process_const_reduction(rr[1], nonterm, parts)
				elif first == 'wrap':
					if len(rr) > 1: raise self._error("invalid 'wrap' reduction")
					rule = self._process_wrap_reduction(nonterm, parts)
				else:
					raise self._error("invalid reduction {!r}", first)
				if union_type is not None:
					# Infer `TUnion.option_types` from reductions
					union_type.option_types.add(rule.reduction.type)
				nonterm.rules.append(rule)
	
	def _process_list_reduction(self, action: str, nonterm: NonTerm, parts: List[Any]) -> Rule:
		assert isinstance(nonterm.type, TList)
		
		rule_len = len(parts)
		if action == 'zero':
			if rule_len > 0:
				raise self._error("invalid 'list zero' reduction")
			reduction: Reduction = RListZero(nonterm.type)
		elif action == 'one':
			if rule_len != 1:
				raise self._error("invalid 'list one' reduction")
			reduction = RListOne(nonterm.type)
		else:
			assert action == 'append'
			if not (2 <= rule_len <= 3):
				raise self._error("invalid 'list append' reduction")
			reduction = RListAppend(rule_len, nonterm.type)
		return Rule([self._get_sym(p[0], optional = p[1]) for p in parts], reduction)
	
	def _process_new_reduction(self, type_str: str, nonterm: NonTerm, parts: List[Any]) -> Rule:
		type = self._get_type(type_str)
		if not isinstance(type, TClass):
			raise self._error("invalid 'new' reduction: type must be a class")
		body: List[Sym] = []
		field_indices: Dict[str, int] = {}
		field_types = type.field_types
		for i, (sym_name, optional, tag_name) in enumerate(parts):
			if tag_name:
				field_type = field_types.get(tag_name)
				if field_type is None:
					raise self._error("{!r} is not a field on class {}", tag_name, type.name)
				if tag_name in field_indices:
					raise self._error("field {!r} assigned to more than one symbol", tag_name)
				field_indices[tag_name] = i
			else:
				field_type = None
			
			if sym_name or field_type is None:
				sym = self._get_sym(sym_name, optional = optional)
			else:
				sym_opt = self._infer_sym(field_type)
				if sym_opt is None:
					raise self._error("sym for field {!r} cannot be inferred", tag_name)
				else:
					sym = sym_opt
			
			if field_type:
				# TODO: Check that sym.type <= field_type
				pass
			
			body.append(sym)
		
		for field_name, field_type in field_types.items():
			if field_name in field_indices: continue
			if isinstance(field_type, TOptional): continue
			raise self._error("non-optional field {!r} unassigned", field_name)
		
		reduction = RNew(len(parts), type, field_indices)
		return Rule(body, reduction)
	
	def _process_const_reduction(self, type_str: str, nonterm: NonTerm, parts: List[Any]) -> Rule:
		type = self._get_type(type_str)
		reduction = RConst(len(parts), type)
		return Rule([self._get_sym(p[0], optional = p[1]) for p in parts], reduction)
	
	def _process_wrap_reduction(self, nonterm: NonTerm, parts: List[Any]) -> Rule:
		type = None
		target_index = None
		for i, (sym_name, optional, tag_name) in enumerate(parts):
			sym = self._get_sym(sym_name, optional = optional)
			if not isinstance(sym, NonTerm): continue
			type = sym.type
			target_index = i
			break
		if target_index is None:
			if len(parts) == 0:
				raise self._error("cannot find target for 'wrap' reduction")
			type = self._get_sym(parts[0][0], optional = parts[0][1]).type
			target_index = 0
		assert type is not None
		reduction = RWrap(len(parts), type, target_index)
		return Rule([self._get_sym(p[0], optional = p[1]) for p in parts], reduction)
	
	def _create_list_rules(self) -> None:
		added_nonterms = {}
		
		for name, data in self.nonterms_raw.items():
			lineno = data['lineno']
			ts = data['typespec']
			if len(ts[0]) < 1: continue
			if ts[0][0] != 'list': continue
			if data['rules']:
				raise self._error("'list' nonterms may not define rules")
			
			item_sym_name = ts[0][1]
			item = [(item_sym_name, False, None)]
			
			sep_sym_name = ts[1].get('sep')
			if sep_sym_name:
				sep = [(sep_sym_name, False, None)]
				sep_opt = [(sep_sym_name, True, None)]
			else:
				sep = []
				sep_opt = []
			
			nonempty = (ts[1].get('nonempty') == 'true')
			trailing = (ts[1].get('trailing') == 'true')
			
			if nonempty:
				# list:
				# | a
				# | list sep a
				data['rules'].extend(_nonempty_list_rules(lineno, name, item, sep))
			else:
				# list:
				# |
				# 	list_zero
				# | list_nonempty sep? # for trailing
				# list_nonempty: ...
				name_nonempty = _new_nonterm_name(name, 'nonempty')
				data['rules'].extend([
					{
						'lineno': lineno,
						'parts': [],
						'reduction': 'list zero',
					},
					{
						'lineno': lineno,
						'parts': [(name_nonempty, False, None)] + (sep_opt if trailing else []),
						'reduction': 'wrap',
					},
				])
				added_nonterms[name_nonempty] = {
					'lineno': lineno,
					'typespec': _parse_arglike('list ' + item_sym_name),
					'rules': _nonempty_list_rules(lineno, name_nonempty, item, sep),
				}
		
		self.nonterms_raw.update(added_nonterms)
	
	def _get_sym(self, name: Optional[str], *, optional: bool = False) -> Sym:
		if not name:
			raise self._error("sym cannot be inferred")
		if optional:
			sym = self._get_sym(name)
			name_opt = _new_nonterm_name(name, 'opt')
			if name_opt not in self.symbols:
				self.symbols[name_opt] = NonTerm(name_opt, self._get_optional_type(sym.type), [
					Rule([], RConst(0, TNull)),
					Rule([sym], RWrap(1, sym.type, 0)),
				])
			name = name_opt
		if name in self.symbols:
			return self.symbols[name]
		if name not in self.nonterms_raw:
			self.symbols[name] = Term(name)
			return self.symbols[name]
		
		data = self.nonterms_raw[name]
		lineno = data['lineno']
		if name in self.type_build_stack:
			raise self._error("circular type dependency {!r}", self.type_build_stack, lineno = lineno)
		
		self.type_build_stack.append(name)
		ts = data['typespec']
		if not ts[0]:
			raise self._error("invalid nonterm typespec", lineno = lineno)
		first = ts[0][0]
		if first in ('class', 'union') and name in self.named_types:
			raise self._error("{} {!r} defined multiple times", first, name, lineno = lineno)
		if first == 'class':
			if len(ts[0]) > 2:
				raise self._error("invalid 'class' typespec", lineno = lineno)
			if len(ts[0]) > 1:
				# appease typechecker
				parent_tmp = self._get_type(ts[0][1])
				if not isinstance(parent_tmp, TClass):
					raise self._error("class parent must also be a class", lineno = lineno)
				parent: Optional[TClass] = parent_tmp
			else:
				parent = None
			t: TSyntax = TClass(name, parent, {})
			self.named_types[name] = t
		elif first == 'union':
			if len(ts[0]) != 1:
				raise self._error("invalid 'union' typespec", lineno = lineno)
			t = TUnion(name, set())
			self.named_types[name] = t
		elif first == 'list':
			if len(ts[0]) != 2:
				raise self._error("invalid 'list' typespec", lineno = lineno)
			item_type = self._get_sym(ts[0][1]).type
			t = self._get_list_type(item_type)
		else:
			t = self._get_type(first)
		self.type_build_stack.pop()
		
		sym = NonTerm(name, t, [])
		self.nonterms[name] = sym
		self.symbols[name] = sym
		return sym
	
	def _infer_sym(self, type: TSyntax) -> Optional[Sym]:
		if isinstance(type, TOptional):
			return self._infer_sym(type.value_type)
		if isinstance(type, TClass):
			return self._get_sym(type.name)
		if isinstance(type, TUnion):
			return self._get_sym(type.name)
		return None
	
	def _get_type(self, name: str) -> TSyntax:
		if name.endswith('?'):
			t = self._get_type(name[:-1])
			return self._get_optional_type(t)
		if name.endswith('[]'):
			t = self._get_type(name[:-2])
			return self._get_list_type(t)
		if name in self.builtin_types:
			return self.builtin_types[name]
		# In this case, it references a named type; and a named type has a nonterm with the same name
		if name not in self.nonterms_raw:
			raise self._error("{!r} is not a named type", name)
		return self._get_sym(name).type
	
	def _get_optional_type(self, t: TSyntax) -> TOptional:
		if isinstance(t, TOptional):
			return t
		if t not in self.optional_types:
			self.optional_types[t] = TOptional(t)
		return self.optional_types[t]
	
	def _get_list_type(self, t: TSyntax) -> TList:
		if t not in self.list_types:
			self.list_types[t] = TList(t)
		return self.list_types[t]
	
	def _set_lineno(self, lineno: int) -> None:
		self.lineno = lineno
	
	def _error(self, message: str, *args: Any, lineno: Optional[int] = None) -> SpecParseError:
		message = message.format(*args)
		if lineno is None:
			lineno = self.lineno
		return SpecParseError("error at {}:{}: {}".format(self.source, lineno+1, message))

def _nonempty_list_rules(lineno: int, name: str, item: List[Any], sep: List[Any]) -> List[Any]:
	return [
		{
			'lineno': lineno,
			'parts': item,
			'reduction': 'list one',
		},
		{
			'lineno': lineno,
			'parts': [(name, False, None)] + sep + item,
			'reduction': 'list append',
		},
	]

def _new_nonterm_name(base: str, suffix: str) -> str:
	return '{}.{}'.format(base, suffix)

def _parse_arglike(s: str) -> Tuple[List[str], Dict[str, str]]:
	args: List[str] = []
	kwargs: Dict[str, str] = {}
	for x in s.split():
		kv = x.split(':', 1)
		if len(kv) == 1:
			args.append(kv[0])
		else:
			kwargs[kv[0]] = kv[1]
	return args, kwargs

def _split_spaces(s: str) -> List[str]:
	return list(filter(bool, re.split(r'\s+', s.strip())))

def _split_rulepart(s: str) -> Tuple[Optional[str], bool, Optional[str]]:
	sym_name_opt, tag_name = _split_two(s, ':')
	if sym_name_opt:
		if sym_name_opt.endswith('?'):
			sym_name: Optional[str] = sym_name_opt[:-1]
			optional = True
		else:
			sym_name = sym_name_opt
			optional = False
	else:
		sym_name = None
		optional = False
	return (sym_name or None, optional, tag_name or None)

def _split_two(s: str, c: str, *, empty_value: str = '') -> Tuple[str, str]:
	s = s.strip()
	if s[0] == '\'':
		b = (_index(s, '\'', 1) or 0) + 1
	else:
		b = 0
	
	i = _index(s, c, b)
	if i is None:
		return (s or empty_value), empty_value
	return s[:i].strip() or empty_value, s[i+1:].strip() or empty_value

def _index(s: str, c: str, beg: int = 0) -> Optional[int]:
	try:
		return s.index(c, beg)
	except ValueError:
		return None
