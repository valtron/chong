from typing import Tuple, NewType, Dict, Set, Any, Sequence, List, Optional, FrozenSet, Union
from collections import defaultdict

from .models import Term, NonTerm, Rule, Sym, Reduction
from .specfile import Spec

StateID = NewType('StateID', int)
AcceptStateID = StateID(-1)

class Shift:
	__slots__ = ('next_state',)
	
	next_state: StateID
	
	def __init__(self, next_state: StateID) -> None:
		self.next_state = next_state
	
	def __eq__(self, other: object) -> bool:
		if not isinstance(other, Shift): return False
		if self.next_state != other.next_state: return False
		return True
	
	def __hash__(self) -> int:
		return hash(self.next_state)
	
	def __repr__(self) -> str:
		return '<Shift({!r})>'.format(self.next_state)

class Reduce:
	__slots__ = ('nonterm', 'reduction')
	
	nonterm: NonTerm
	reduction: Any
	
	def __init__(self, nonterm: NonTerm, reduction: Any) -> None:
		self.nonterm = nonterm
		self.reduction = reduction
	
	def __eq__(self, other: object) -> bool:
		if not isinstance(other, Reduce): return False
		if self.nonterm != other.nonterm: return False
		if self.reduction != other.reduction: return False
		return True
	
	def __hash__(self) -> int:
		return hash((self.nonterm, self.reduction))
	
	def __repr__(self) -> str:
		return '<Reduce({!r}, {!r})>'.format(self.nonterm, self.reduction)

Action = Union[Shift, Reduce]
ActionDict = Dict[Tuple[StateID, Term], Set[Action]]
GotoDict = Dict[Tuple[StateID, NonTerm], StateID]

class ParseTables:
	__slots__ = ('actions', 'goto')
	
	actions: ActionDict
	goto: GotoDict
	
	@classmethod
	def FromSpec(cls, spec: Spec) -> 'ParseTables':
		return cls.FromSymbols(spec.symbols, spec.start)
	
	@classmethod
	def FromSymbols(cls, symbols: Dict[str, Sym], start: str) -> 'ParseTables':
		return _create_parse_tables(symbols, start)
	
	def __init__(self, actions: ActionDict, goto: GotoDict) -> None:
		self.actions = actions
		self.goto = goto

def _create_parse_tables(symbols: Dict[str, Sym], start: str) -> ParseTables:
	start_sym = symbols[start]
	start_aux = NonTerm('__START', start_sym.type, [
		Rule([start_sym, Term.End], _DummyReduction(2, start_sym.type)),
	])
	s0 = closure([Item(start_aux, 0)])
	
	actions: ActionDict = defaultdict(set)
	goto: GotoDict = {}
	
	stack = [s0]
	itemsets = { s0: StateID(0) }
	while stack:
		s = stack.pop()
		
		ss: Dict[Sym, List[Item]] = defaultdict(list)
		
		for i in s:
			sym = i.nextSymbol()
			if not sym: continue
			ss[sym].append(i.advance())
		
		for sym, iset in ss.items():
			fs = closure(iset)
			
			if fs not in itemsets:
				itemsets[fs] = StateID(len(itemsets))
				stack.append(fs)
			
			sa = itemsets[s]
			sb = itemsets[fs]
			
			if isinstance(sym, Term):
				sid = (AcceptStateID if sym == Term.End else sb)
				actions[(sa, sym)].add(Shift(sid))
			else:
				goto[(sa, sym)] = sb
	
	nonterms = [sym for sym in symbols.values() if isinstance(sym, NonTerm)]
	nonterms.append(start_aux)
	follows = compute_follows(nonterms)
	
	for s, sid in itemsets.items():
		for i in s:
			if i.nextSymbol(): continue
			for tok in follows[i.nonterm]:
				k_term = (sid, tok)
				actions[k_term].add(Reduce(i.nonterm, i.rule.reduction))
	
	return ParseTables(actions, goto)

class _DummyReduction(Reduction):
	__slots__ = ()

class Item:
	__slots__ = ('nonterm', 'rule_index', 'rule', 'pos')
	
	nonterm: NonTerm
	rule_index: int
	rule: Rule
	pos: int
	
	def __init__(self, nonterm: NonTerm, rule_index: int, pos: int = 0) -> None:
		self.nonterm = nonterm
		self.rule_index = rule_index
		self.rule = nonterm.rules[rule_index]
		self.pos = pos
	
	def advance(self) -> 'Item':
		assert self.pos < len(self.rule.body)
		return Item(self.nonterm, self.rule_index, self.pos + 1)
	
	def nextSymbol(self) -> Optional[Sym]:
		k = self.pos
		b = self.rule.body
		if k >= len(b):
			return None
		return b[k]
	
	def __eq__(self, other: object) -> bool:
		if not isinstance(other, Item): return False
		return self.tuple() == other.tuple()
	
	def __hash__(self) -> int:
		return hash(self.tuple())
	
	def tuple(self) -> Tuple[str, int, int]:
		return (self.nonterm.name, self.rule_index, self.pos)
	
	def dots(self) -> str:
		b = self.rule.body
		c = self.pos
		pre = ' '.join(s.name for s in b[:c])
		post = ' '.join(s.name for s in b[c:])
		return '{} -> {} . {}'.format(self.nonterm.name, pre, post).replace('  ', ' ')

def closure(s: Sequence[Item]) -> FrozenSet[Item]:
	ns = set(s)
	
	while True:
		to_add = set()
		
		for i in ns:
			sym = i.nextSymbol()
			
			if sym is None: continue
			if not isinstance(sym, NonTerm): continue
			
			for idx in range(len(sym.rules)):
				j = Item(sym, idx)
				if j not in ns:
					to_add.add(j)
		
		if not to_add: break
		
		ns |= to_add
	
	return frozenset(ns)

def compute_follows(nonterms: List[NonTerm]) -> Dict[NonTerm, Set[Term]]:
	nullable: Set[NonTerm] = set()
	while True:
		changed = False
		for nt in nonterms:
			if nt in nullable:
				continue
			for r in nt.rules:
				n = True
				for sym in r.body:
					if isinstance(sym, Term) or sym not in nullable:
						n = False
						break
				if n:
					if nt not in nullable:
						nullable.add(nt)
						changed = True
					break
		if not changed:
			break
	
	first: Dict[NonTerm, Set[Term]] = { nt: set() for nt in nonterms }
	while True:
		changed = False
		for nt in nonterms:
			for r in nt.rules:
				for sym in r.body:
					if isinstance(sym, Term):
						if sym not in first[nt]:
							first[nt].add(sym)
							changed = True
					else:
						for sym_first in first[sym]:
							if sym_first not in first[nt]:
								first[nt].add(sym_first)
								changed = True
					if sym not in nullable:
						break
		if not changed:
			break
	
	follows: Dict[NonTerm, Set[Term]] = { nt: set() for nt in nonterms }
	while True:
		changed = False
		for nt in nonterms:
			for r in nt.rules:
				for i, sym1 in enumerate(r.body):
					if not isinstance(sym1, NonTerm):
						continue
					reached_end = True
					for j, sym2 in enumerate(r.body[i+1:]):
						if isinstance(sym2, Term):
							if sym2 not in follows[sym1]:
								follows[sym1].add(sym2)
								changed = True
						else:
							for sym_first in first[sym2]:
								if sym_first not in follows[sym1]:
									follows[sym1].add(sym_first)
									changed = True
						
						if sym2 not in nullable:
							reached_end = False
							break
					
					if reached_end:
						for sym_follows in follows[nt]:
							if sym_follows not in follows[sym1]:
								follows[sym1].add(sym_follows)
								changed = True
		if not changed:
			break
	
	return follows

def print_itemsets(itemsets: Dict[Set[Item], StateID]) -> None:
	for itemset, id in itemsets.items():
		print("Itemset", id)
		lines = [item.dots() for item in itemset]
		for line in sorted(lines):
			print(line)
		print("")
