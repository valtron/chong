from typing import Dict, Optional, Set, List, Any, ClassVar, Union

# TODO: Make more use of `Union`

class TSyntax:
	__slots__ = ()
	
	# Hacky
	def __repr__(self) -> str:
		return '<TToken>'

TToken = TSyntax()
TNull = TSyntax()

class TList(TSyntax):
	__slots__ = ('item_type',)
	
	item_type: TSyntax
	
	def __init__(self, item_type: TSyntax) -> None:
		super().__init__()
		self.item_type = item_type
	
	def __repr__(self) -> str:
		return '<TList({!r})>'.format(self.item_type)

class TClass(TSyntax):
	__slots__ = ('name', 'parent', 'field_types',)
	
	name: str
	parent: Optional['TClass']
	field_types: Dict[str, TSyntax]
	
	def __init__(self, name: str, parent: Optional['TClass'], field_types: Dict[str, TSyntax]) -> None:
		super().__init__()
		self.name = name
		self.parent = parent
		self.field_types = field_types
	
	def __repr__(self) -> str:
		return '<TClass({!r}, {!r}, {!r})>'.format(self.name, self.parent, self.field_types)

class TUnion(TSyntax):
	__slots__ = ('name', 'option_types',)
	
	name: str
	option_types: Set[TSyntax]
	
	def __init__(self, name: str, option_types: Set[TSyntax]) -> None:
		super().__init__()
		self.name = name
		self.option_types = option_types
	
	def __repr__(self) -> str:
		return '<TUnion({!r}, {!r})>'.format(self.name, self.option_types)

class TOptional(TSyntax):
	__slots__ = ('value_type',)
	
	value_type: TSyntax
	
	def __init__(self, value_type: TSyntax) -> None:
		super().__init__()
		self.value_type = value_type
	
	def __repr__(self) -> str:
		return '<TOptional({!r})>'.format(self.value_type)

class Reduction:
	__slots__ = ('rule_len', 'type')
	
	rule_len: int
	type: TSyntax
	
	def __init__(self, rule_len: int, type: TSyntax) -> None:
		self.rule_len = rule_len
		self.type = type
	
	def __repr__(self) -> str:
		return '<{}>'.format(type(self).__name__)

class RWrap(Reduction):
	__slots__ = ('prefix_index', 'target_index', 'postfix_index')
	
	prefix_index: int
	target_index: int
	postfix_index: int
	
	def __init__(self, rule_len: int, type: TSyntax, target_index: int) -> None:
		super().__init__(rule_len, type)
		self.prefix_index = 0
		self.target_index = target_index
		self.postfix_index = target_index + 1

class RListZero(Reduction):
	__slots__ = ()
	
	def __init__(self, type: TList) -> None:
		super().__init__(0, type)

class RListOne(Reduction):
	__slots__ = ('item_index',)
	
	item_index: int
	
	def __init__(self, type: TList) -> None:
		super().__init__(1, type)
		self.item_index = 0

class RListAppend(Reduction):
	__slots__ = ('list_index', 'sep_index', 'item_index')
	
	list_index: int
	sep_index: Optional[int]
	item_index: int
	
	def __init__(self, rule_len: int, type: TList) -> None:
		assert rule_len in (2, 3)
		super().__init__(rule_len, type)
		self.list_index = 0
		self.sep_index = 1 if rule_len == 3 else None
		self.item_index = (rule_len - 1)

class RConst(Reduction):
	__slots__ = ()

class RNew(Reduction):
	__slots__ = ('field_indices',)
	
	field_indices: Dict[str, int]
	
	def __init__(self, rule_len: int, type: TClass, field_indices: Dict[str, int]) -> None:
		super().__init__(rule_len, type)
		self.field_indices = field_indices

Sym = Union['Term', 'NonTerm']

class Term:
	__slots__ = ('name', 'type')
	
	End: ClassVar['Term'] = None # type: ignore
	
	name: str
	type: TSyntax
	
	def __init__(self, name: str) -> None:
		self.name = name
		self.type = TToken
	
	def __repr__(self) -> str:
		return '<Term({!r})>'.format(self.name)
Term.End = Term('$')

class Rule:
	__slots__ = ('body', 'reduction')
	
	body: List[Sym]
	reduction: Reduction
	
	def __init__(self, body: List[Sym], reduction: Reduction) -> None:
		self.body = body
		self.reduction = reduction

class NonTerm:
	__slots__ = ('name', 'type', 'rules',)
	
	name: str
	type: TSyntax
	rules: List[Rule]
	
	def __init__(self, name: str, type: TSyntax, rules: List[Rule]) -> None:
		self.name = name
		self.type = type
		self.rules = rules
	
	def __repr__(self) -> str:
		return '<NonTerm({!r})>'.format(self.name)
